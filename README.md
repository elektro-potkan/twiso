TWISO Driver = Two-Wire-Interface Slave-Only Driver
===================================================
Driver for a group of Atmel/Microchip AVR MCUs with simplified Slave-Only Two-Wire-Interface (like the ATtiny40).

For other MCUs with full-featured Two-Wire-Interface, there is a driver from Atmel AVR311 / Microchip AN_2565 available.

Compiling
---------
Compiler and its parameters used during development:
```bash
avr-gcc -o twiso.o -std=gnu99 -mmcu=<MCU> -DF_CPU=<MCU-FREQUENCY> -Wall -Werror -pedantic -g2 -gstabs -Os -fpack-struct -fshort-enums -funsigned-char -funsigned-bitfields -c src/twiso.c
```

Makefile target:
```make
twiso.o: src/twiso.c src/twiso.h
	avr-gcc -o twiso.o -std=gnu99 -mmcu=<MCU> -DF_CPU=<MCU-FREQUENCY> -Wall -Werror -pedantic -g2 -gstabs -Os -fpack-struct -fshort-enums -funsigned-char -funsigned-bitfields -c src/twiso.c
```

Configuration
-------------
To change the buffer length, define the constant `TWISO_BUFFER_LENGTH` before including the TWISO header.
However, this will change its value for the current module only.
You need to define it also when compiling the TWISO module itself.
The recommended way is to add it in the compiler flags.

For `avr-gcc` use the `-D` argument:
```bash
avr-gcc ... -DTWISO_BUFFER_LENGTH=<length>
```

License
-------
This program is licensed under the MIT License.

See file [LICENSE](LICENSE).

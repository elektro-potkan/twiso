/**
***********************************************
***       AVR TWI Slave-Only (Driver)       ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***               MIT License               ***
***********************************************
*/


/**
 * Driver for TWI Slave-Only interface available in some Atmel/Microchip AVR MCUs (e.g. ATtiny40)
 */
#ifndef TWISO_H_
#define TWISO_H_


// Test MCU support first
#if !defined(__AVR_ATtiny40__)
	#error "TWISO: The selected MCU, you are compiling for, seems to be unsupported by this driver! \
If you think, that it should be supported, please feel free to verify it and push merge request. \
Note however, that this driver is intended only for the group of MCUs with simplified Slave-Only Two-Wire-Interface (like the ATtiny40). \
The full-featured Two-Wire-Interface is supported by the driver from Atmel AVR311 / Microchip AN_2565."
#endif


#include <inttypes.h>


/** Driver version */
#define TWISO_VERSION_   "1.1.2"

/** Size of internal buffer (bytes) */
#ifndef TWISO_BUFFER_LENGTH
	#define TWISO_BUFFER_LENGTH   8
#endif


/**
 * Init driver
 * @param addr - primary 7-bit address
 * @param gen - whether to react on general-call
 * @param autostart - whether to left the driver running after all data has been send
 * @param addr2 - secondary 7-bit adress (or mask) - not used if zero
 * @param addr2mask - treat addr2 as a bit-mask of primary address instead of secondary address
 * @param sda_hold - if true, enable the insertion of ~50ns delay after each negative transition of SCL before the device is allowed to change the SDA line
 */
void twiso_init(uint8_t addr, unsigned char gen, unsigned char autostart, uint8_t addr2, unsigned char addr2mask, unsigned char sda_hold);
inline
	void twiso_init_dual(uint8_t addr, unsigned char gen, unsigned char autostart, uint8_t addr2, unsigned char addr2mask){twiso_init(addr, gen, autostart, addr2, addr2mask, 0);}
inline
	void twiso_init_basic(uint8_t addr, unsigned char gen, unsigned char autostart){twiso_init(addr, gen, autostart, 0, 0, 0);}

/**
 * Starts transceiver (optionally with data to be read by master)
 * @param data - pointer to data to be copied to internal buffer for next read transaction(s)
 *   - If the next transaction will be write, the buffer will be overwritten
 *     and nothing will be send as there is only single buffer for both directions.
 *   - The master could issue more than one transaction to read the data (the driver will continue until it will send all data).
 *     However, as noted above, first write transaction (containing some data) will overwrite the buffer.
 * @param length - data length
 *   - Be aware of buffer length setting above - if you will pass more data than buffer size,
 *     this function will silently fail (but will not change (broke) anything).
 */
void twiso_start(uint8_t* data, unsigned char length);
inline
	void twiso_start_nodata(){twiso_start(0, 0);}
inline
	void twiso_start_byte(uint8_t data){twiso_start(&data, 1);}
inline
	void twiso_start_word(uint16_t data){twiso_start((uint8_t*) &data, 2);}
inline
	void twiso_start_string(char* data, unsigned char length){twiso_start((uint8_t*) data, length);}

/**
 * Returns whether the Driver is busy transceiving data (e.g. the internal buffer is being modified)
 * @return true if busy
 */
unsigned char twiso_isBusy();

/**
 * Retrieves data from internal buffer
 * @param data - pointer to location, to which the data should be copied
 * @param length - maximum data length to copy
 * @return number of retrieved bytes if arguments valid
 *      or number of bytes in internal buffer if not (null pointer or zero length given)
 */
unsigned char twiso_getData(uint8_t* data, unsigned char length);
inline
	unsigned char twiso_getDataLength(){return twiso_getData(0, 0);}


#endif

/**
***********************************************
***       AVR TWI Slave-Only (Driver)       ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***               MIT License               ***
***********************************************
*/


#include "twiso.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <inttypes.h>


/**
***********************
***** DEFINITIONS *****
***********************
*/

/** Booleans */
#define FALSE 0
#define TRUE 1

#define BOOL(var) ((var) ? TRUE : FALSE)
//#define BOOL(var) (!(!(var)))


/** Registers shifting */
#define TWSA_ADDR   TWSA1
#define TWSA_GEN    TWSA0
#define TWSAM_ADDR   TWSAM1


/** Acknowledges */
#define ACK    0
#define NACK   1

/** TWI States */
#define TWISO_STATE_READ_ADDR_ACK    (1 << TWDIR) | (1 << TWASIF) | (1 << TWAS) | (1 << TWCH) | (ACK << TWRA)
#define TWISO_STATE_READ_ADDR_NACK   (1 << TWDIR) | (1 << TWASIF) | (1 << TWAS) | (1 << TWCH) | (NACK << TWRA)
#define TWISO_STATE_READ_DATA_ACK    (1 << TWDIR) | (1 << TWDIF)  | (1 << TWAS) | (1 << TWCH) | (ACK << TWRA)
#define TWISO_STATE_READ_DATA_NACK   (1 << TWDIR) | (1 << TWDIF)  | (1 << TWAS) | (1 << TWCH) | (NACK << TWRA)
#define TWISO_STATE_READ_STOP_ACK    (1 << TWDIR) | (1 << TWASIF)                             | (ACK << TWRA)
#define TWISO_STATE_READ_STOP_NACK   (1 << TWDIR) | (1 << TWASIF)                             | (NACK << TWRA)
#define TWISO_STATE_WRITE_ADDR_ACK                  (1 << TWASIF) | (1 << TWAS) | (1 << TWCH) | (ACK << TWRA)
#define TWISO_STATE_WRITE_ADDR_NACK                 (1 << TWASIF) | (1 << TWAS) | (1 << TWCH) | (NACK << TWRA)
#define TWISO_STATE_WRITE_DATA_ACK                  (1 << TWDIF)  | (1 << TWAS) | (1 << TWCH) | (ACK << TWRA)
#define TWISO_STATE_WRITE_DATA_NACK                 (1 << TWDIF)  | (1 << TWAS) | (1 << TWCH) | (NACK << TWRA)
#define TWISO_STATE_WRITE_STOP_ACK                  (1 << TWASIF)                             | (ACK << TWRA)
#define TWISO_STATE_WRITE_STOP_NACK                 (1 << TWASIF)                             | (NACK << TWRA)

/** TWI State masks */
#define TWISO_STATE_MASK_COLLISION   (1 << TWC)
#define TWISO_STATE_MASK_ERROR   (1 << TWBE)

/** Commands */
#define TWCMD_ACK    (ACK << TWAA)// set ACK as acknowledge action
#define TWCMD_NACK   (NACK << TWAA)// set NACK as acknowledge action
#define TWCMD_RESET     (2 << TWCMD0)// 0b10 // reset TWI to wait for next START condition (used to complete transaction)
#define TWCMD_PROCEED   (3 << TWCMD0)// 0b11 // proceed to next byte (used in the middle of transaction)



/**
******************************
***** INTERNAL VARIABLES *****
******************************
*/

/** Internal flags */
volatile unsigned char twiso_flags = 0;

/** Flags bits */
#define TWISO_FLAGS_CONFIGURED   (1 << 0)// driver configured
#define TWISO_FLAGS_BUSY         (1 << 1)// we are busy sending/receiving data
#define TWISO_FLAGS_AUTOSTART    (1 << 2)// whether to left driver running after all data has been send to master
#define TWISO_FLAGS_FIRST        (1 << 3)// we are in the state before first data byte (we just received the ADDR)
#define TWISO_FLAGS_UNBUSIED     (1 << 4)// we already unset the busy flag, so do not reset it again during this transaction (it might be already set again by request to send data)
#define TWISO_FLAGS_INWRITE      (1 << 5)// we are inside of the WRITE transaction and expect it to continue; used to cancel the busy flag by repeated start condition

/** Last errors */
#define TWISO_FLAGS_COLLISION    (1 << 6)
#define TWISO_FLAGS_ERROR        (1 << 7)


/** State register value from last error */
volatile uint8_t twiso_state = 0;


/** Internal buffer */
#if TWISO_BUFFER_LENGTH < 1 || TWISO_BUFFER_LENGTH > 255
	#error "TWISO: Bad length of buffer!"
#endif

volatile uint8_t twiso_buffer[TWISO_BUFFER_LENGTH];

/** Index of actual buffer byte */
volatile unsigned char twiso_buffer_index = 0;

/** Actual length of data in buffer */
volatile unsigned char twiso_buffer_length = 0;



/**
****************************
***** INTERNAL HELPERS *****
****************************
*/

/**
 * Returns maximum value from given arguments
 * @param a
 * @param b
 * @return maximum
 */
inline unsigned char max(unsigned char a, unsigned char b){
	return (a > b) ? a : b;
} // max

/**
 * Returns minimum value from given arguments
 * @param a
 * @param b
 * @return minimum
 */
inline unsigned char min(unsigned char a, unsigned char b){
	return (a < b) ? a : b;
} // min


/**
 * Enables TWI Interrupts (TWI Data + TWI Address/Stop + TWI Stop)
 */
inline void twiso_twi_enable(){
	TWSCRA |= (1 << TWDIE) | (1 << TWASIE) | (1 << TWSIE);
} // twiso_twi_enable

/**
 * Disables TWI Interrupts (TWI Data + TWI Address/Stop + TWI Stop)
 */
inline void twiso_twi_disable(){
	TWSCRA &= ~((1 << TWDIE) | (1 << TWASIE) | (1 << TWSIE));
} // twiso_twi_disable



/**
*********************
***** FUNCTIONS *****
*********************
*/

void twiso_init(uint8_t addr, unsigned char gen, unsigned char autostart, uint8_t addr2, unsigned char addr2mask, unsigned char sda_hold){
	// ensure the interrupt is disabled
	TWSCRA = 0;
	
	// reset interface status
	TWSSRA = 0xFF;
	
	// reset internal variables
	twiso_flags = 0;
	twiso_buffer_index = 0;
	twiso_buffer_length = 0;
	
	// configure
	if(autostart){
		twiso_flags |= TWISO_FLAGS_AUTOSTART;
	};
	
	// set primary address
	TWSA = (addr << TWSA_ADDR)
	     | ((!addr || gen) << TWSA_GEN);// enable general call also if addres is zero
	
	// set secondary address / primary address mask
	TWSAM = (addr2 << TWSAM_ADDR)
	      | ((!addr2mask) << TWAE);
	
	// setup interface
	TWSCRA = (1 << TWEN) // enable interface
	       | (BOOL(sda_hold) << TWSHE) // setting of SDA Hold Time Enable
	       | (1 << TWSME);// enable Smart Mode acknowledges
	
	TWSCRB = (NACK << TWAA);// send NACK by default
	
	// allow interaction with driver
	twiso_flags |= TWISO_FLAGS_CONFIGURED;
} // twiso_init

void twiso_start(uint8_t* data, unsigned char length){
	// check driver state and data length
	if(!(twiso_flags & TWISO_FLAGS_CONFIGURED) || length > TWISO_BUFFER_LENGTH){
		return;
	};
	
	// wait until we are ready to accept new commands
	while(twiso_flags & TWISO_FLAGS_BUSY);
	
	// copy data to buffer, if any provided
	if(length > 0 && data){
		// pause the TWI for the while we need to copy the data
		twiso_twi_disable();
		
		// we will be busy after copying data to ("output") buffer
		twiso_flags |= TWISO_FLAGS_BUSY;
		
		for(unsigned char i = 0; i < length; i++){
			twiso_buffer[i] = data[i];
		};
		
		twiso_buffer_length = length;
	}
	else {
		twiso_buffer_length = 0;
	};
	
	twiso_buffer_index = 0;
	
	// start TWI
	twiso_twi_enable();
} // twiso_start

unsigned char twiso_isBusy(){
	return (twiso_flags & TWISO_FLAGS_BUSY);
} // twiso_isBusy

unsigned char twiso_getData(uint8_t* data, unsigned char length){
	// wait until we are ready to return valid data
	while(twiso_flags & TWISO_FLAGS_BUSY);
	
	// handle data
	if(length > 0 && data){// copy data to provided array
		length = min(length, twiso_buffer_length);
		
		for(unsigned char i = 0; i < length; i++){
			data[i] = twiso_buffer[i];
		};
		
		return length;
	}
	else {// null pointer or zero length, return current buffer length only
		return twiso_buffer_length;
	};
} // twiso_getData



/**
*********************************
***** TWI Interrupt routine *****
*********************************
*/
ISR(TWI_SLAVE_vect){
	switch(TWSSRA){
		case TWISO_STATE_WRITE_ADDR_NACK:
		case TWISO_STATE_WRITE_ADDR_ACK:
			// cancel the in-write flag (this is repeated-start condition)
			if(twiso_flags & TWISO_FLAGS_INWRITE){
				twiso_flags &= ~(TWISO_FLAGS_BUSY | TWISO_FLAGS_INWRITE);
			};
			
			// reset errors
			twiso_flags &= ~(TWISO_FLAGS_COLLISION | TWISO_FLAGS_ERROR);//TODO unsafe, but is there a better way?
			
			// prepare for data
			twiso_flags |= TWISO_FLAGS_FIRST;
			
			// send ACK and continue to data byte
			TWSCRB = TWCMD_ACK | TWCMD_PROCEED;
			break;
		case TWISO_STATE_WRITE_DATA_NACK:
		case TWISO_STATE_WRITE_DATA_ACK:
			// we are busy receiving data
			twiso_flags |= TWISO_FLAGS_BUSY | TWISO_FLAGS_INWRITE;
			
			// prepare for data if we are receiving first byte
			if(twiso_flags & TWISO_FLAGS_FIRST){
				// reset index pointer
				twiso_buffer_index = 0;
				
				// reset flag
				twiso_flags &= ~TWISO_FLAGS_FIRST;
			};
			
			// check data length and prepare (N)ACK according to it
			if(twiso_buffer_index >= TWISO_BUFFER_LENGTH - 1){// last byte that could be received
				TWSCRB = TWCMD_NACK;
			};
			
			// save data, will trigger the (N)ACK to be send - ACK remains from previous ADDR Interrupt, optional NACK set above
			if(twiso_buffer_index < TWISO_BUFFER_LENGTH){// everything OK
				twiso_buffer[twiso_buffer_index++] = TWSD;
			}
			else {// internal error - something bad happened as we would make an illegal write to memory (no space to write the data to)
				volatile uint8_t null = TWSD;// touch the register to trigger the NACK to be send (will it be send even without it when we enabled the Smart Mode acknowledges?)
				null &= 0;// hack to suppress the unused-variable compiler error
			};
			
			// update data length
			twiso_buffer_length = twiso_buffer_index;
			
			// continue to next byte if enough space, otherwise set TWI to idle until next START condition
			if(twiso_buffer_index < TWISO_BUFFER_LENGTH){// enough space for another byte
				TWSCRB = TWCMD_PROCEED;
			}
			else {// last byte that could be received was received
				TWSCRB = TWCMD_RESET;
				
				// stop driver to keep the buffer until started again
				twiso_twi_disable();
				
				twiso_flags &= ~TWISO_FLAGS_BUSY;
				twiso_flags &= ~TWISO_FLAGS_INWRITE;
			};
			break;
		case TWISO_STATE_WRITE_STOP_NACK:
		case TWISO_STATE_WRITE_STOP_ACK:
			// stop driver, but only in case we received some data (we might unnecessary drop data to be transmitted if we will set length to zero)
			if(!(twiso_flags & TWISO_FLAGS_FIRST)){
				// stop driver to keep the buffer until started again
				twiso_twi_disable();
				
				// end of transaction, so we are not busy nor in the middle of transaction
				twiso_flags &= ~TWISO_FLAGS_BUSY;
				twiso_flags &= ~TWISO_FLAGS_INWRITE;
			};
			
			TWSSRA |= TWASIF;
			break;
		case TWISO_STATE_READ_ADDR_NACK:
		case TWISO_STATE_READ_ADDR_ACK:
			twiso_flags &= ~TWISO_FLAGS_UNBUSIED;
			
			// cancel the in-write flag (this is repeated-start condition)
			if(twiso_flags & TWISO_FLAGS_INWRITE){
				twiso_flags &= ~(TWISO_FLAGS_BUSY | TWISO_FLAGS_INWRITE);
			};
			
			// reset errors
			twiso_flags &= ~(TWISO_FLAGS_COLLISION | TWISO_FLAGS_ERROR);//TODO unsafe, but is there a better way?
			
			// set the first flag, so we know, that we should ignore the NACK in the next (first) data state
			twiso_flags |= TWISO_FLAGS_FIRST;
			
			// check, if we have any data for master
			if(twiso_buffer_length > 0 && twiso_buffer_index < twiso_buffer_length){
				// send ACK and continue to data bytes
				TWSCRB = TWCMD_ACK | TWCMD_PROCEED;
				
				// we should not reset the index as the master could use more READ transactions to read all data
				// index is reset once we are loading new data in buffer (also when receiving)
			}
			else {
				// send NACK and reset to waiting for next START condition
				TWSCRB = TWCMD_NACK | TWCMD_RESET;
			};
			
			break;// the TWDIF will be set automatically, so the DATA part will be executed in the next Interrupt
		case TWISO_STATE_READ_DATA_NACK:// could occur when we are sending the first byte in transaction (as the master NACK is kept in register from previous transaction); if not the first byte, then it is an error condition - we received clocks when we should not
			if(!(twiso_flags & TWISO_FLAGS_FIRST)){// not the first byte in transaction, so the NACK is valid - an error condition occured
				// cancel the transaction until new START condition
				TWSCRB = TWCMD_RESET;// Might be, that the hardware part of TWI throws this interrupt before the STOP interrupt? Then, this will be OK and not an error...
				
				// end of transaction, so we are not busy
				if(!(twiso_flags & TWISO_FLAGS_UNBUSIED)){// but only if it was not done before (as the TWI might be already started again with new data)
					twiso_flags &= ~TWISO_FLAGS_BUSY;//TODO what to do, when master did not read whole buffer? We should not reset it then...
					twiso_flags |= TWISO_FLAGS_UNBUSIED;
				};
				
				break;
			};
			// first byte, proceed to standard send
		case TWISO_STATE_READ_DATA_ACK:
			// we are busy sending data
			twiso_flags |= TWISO_FLAGS_BUSY;
			
			// clear the first flag, so we will consider the NACK in the next data state
			twiso_flags &= ~TWISO_FLAGS_FIRST;
			
			// handle data
			if(twiso_buffer_index < twiso_buffer_length){// everything OK, we have data to send
				TWSD = twiso_buffer[twiso_buffer_index++];
			}
			else {// internal error - something bad happened as we should not be in this interrupt without any data to send
				TWSD = 0;// fill the data register with zeros as we don't have anything to send
			};
			
			// prepare for next phase
			if(twiso_buffer_index < twiso_buffer_length){// we still have data to send
				TWSCRB = TWCMD_PROCEED;
			}
			else {// last byte we could offer
				TWSCRB = TWCMD_RESET;
				
				// finalize transaction
				if(!(twiso_flags & TWISO_FLAGS_AUTOSTART)){// autostart disabled, stop the driver
					twiso_twi_disable();
					
					// we should always reset busy flag when we are stopping the driver
					twiso_flags &= ~TWISO_FLAGS_BUSY;
					twiso_flags |= TWISO_FLAGS_UNBUSIED;
				};
				twiso_buffer_length = 0;
				
				// end of transaction, so we are not busy
				if(!(twiso_flags & TWISO_FLAGS_UNBUSIED)){// but only if it was not done before (as the TWI might be already started again with new data)
					twiso_flags &= ~TWISO_FLAGS_BUSY;
					twiso_flags |= TWISO_FLAGS_UNBUSIED;
				};
			};
			
			break;
		case TWISO_STATE_READ_STOP_NACK:
		case TWISO_STATE_READ_STOP_ACK:
			// nothing to do as all logic was already handled by other READ interrupts
			TWSSRA |= TWASIF;
			break;
		default:
			// detect state
			if(TWSSRA & TWISO_STATE_MASK_COLLISION){// collision detected
				twiso_flags |= TWISO_FLAGS_COLLISION;
			}
			else {// any other state is an error
				twiso_state = TWSSRA;
				twiso_flags |= TWISO_FLAGS_ERROR;
			};
			
			// nothing better to do than reset the interface
			TWSCRB = TWCMD_RESET;//TODO
			twiso_flags &= ~TWISO_FLAGS_BUSY;//TODO unsafe unblocking, but is there a better way?
	};
} // TWI ISR
